# How to run this project ?
1. install nodejs
2. Do a ```git clone https://gitlab.com/leo_george/testserver-for-develpoment.git```
3. ``` cd testserver-for-develpoment/ ```
4. Then do a ```npm install```
5. Visit ```localhost:4000``` 

## How this is helpful ?
while doing a network testing for our app or webapp, sometimes it is good to have a test server running to get data from endpoints. Making our development easier

## Is this enough ?
- [x] Added a json endpoint. 

